wbmachine
=========
wbmachine finds an archived version of a URL from the Internet Archive's Wayback
Machine.  It can also find an archived webpage that is close to a certain date.

Dependencies
------------
These programs are necessary for running wbmachine.
*  curl
*  jq

Installing
----------
Edit config.mk as necessary.  Then, run `make install`.  If required, do so as
root.

Uninstalling
------------
Run `make uninstall`.  If required, do so as root.

Running
-------
See the man page for details on how to run wbmachine.

Copying
-------
Copyright (C) 2020 Andrew Lieberman <alieberman@posteo.net>

Copying and distributing this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
