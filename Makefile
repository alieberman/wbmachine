# Makefile for wbmachine
#
# Copyright (C) 2020 Andrew Lieberman <alieberman@posteo.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include config.mk

.PHONY: install uninstall

install:
	mkdir -p -m $(DIRMODE) $(DESTDIR)$(PREFIX)/bin
	mkdir -p -m $(DIRMODE) $(DESTDIR)$(MANPREFIX)/man1
	mkdir -p -m $(DIRMODE) $(DESTDIR)$(DOCPREFIX)/wbmachine
	install -m $(BINMODE) wbmachine $(DESTDIR)$(PREFIX)/bin
	install -m $(GENERALMODE) wbmachine.1 $(DESTDIR)$(MANPREFIX)/man1
	install -m $(GENERALMODE) LICENSE README.md \
		$(DESTDIR)$(DOCPREFIX)/wbmachine

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/wbmachine
	rm -f $(DESTDIR)$(MANPREFIX)/man1/wbmachine.1
	rm -rf $(DESTDIR)$(DOCPREFIX)/wbmachine
